const et_messages = {
  notice: {
    msg: 'Sul on %{noticeType} sõnum.'
  },
  translations: {
    title: 'Tõlked'
  },
  common: {
    name: 'Nimi',
    remind: 'meeldetuletus',
    short_date: 'Lühike kuupäev:',
    long_date: 'Pikk kuupäev:'
  },
  message: {
    hello: '%{msg} maailm!'
  },
  alertType: 'äratus | error | mitte ühtegi | üks ',
  apple: '0 õuna | 1 õun | {count} õuna'
}

export default et_messages
