import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import VueI18n from 'vue-i18n'
import en_messages from '../locales/en_messages.json'
import et_messages from '../locales/et_messages.js'
import { dateTimeFormats, numberFormats } from '../locales/format'

Vue.config.productionTip = false
Vue.use(VueI18n)

const locale = 'en'

const messages = {
  en: en_messages,
  et: et_messages
}

const i18n = new VueI18n({
  locale,
  fallbackLocale: 'en_messages',
  messages,
  dateTimeFormats,
  numberFormats,
  missing: (locale, key, vm) => {
    console.log(`${key} is missing in ${locale}, ${vm}`)
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
