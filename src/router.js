import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import PlayerList from './views/players/PlayerList.vue'
import PlayerShow from './views/players/PlayerShow.vue'
import PlayerCreate from './views/players/PlayerCreate.vue'
import TeamList from './views/teams/TeamList.vue'
import Translations from './views/Translations.vue'
import UI from './views/UI.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/players',
      name: 'players',
      component: PlayerList
    },
    {
      path: '/player/:id',
      name: 'player-show',
      component: PlayerShow,
      props: true
    },
    {
      path: '/player/create',
      name: 'player-create',
      component: PlayerCreate
    },
    {
      path: '/teams',
      name: 'teams',
      component: TeamList
    },
    {
      path: '/translations',
      name: 'translations',
      component: Translations
    },
    {
      path: '/ui',
      name: 'ui',
      component: UI
    }
  ]
})
