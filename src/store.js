import Vue from 'vue'
import Vuex from 'vuex'
import DatabaseService from '@/services/DatabaseService.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {
      id: 1,
      name: 'Riana'
    },
    players: [],
    playersTotal: 0,
    player: {},
    teams: []
  },
  mutations: {
    ADD_PLAYER(state, player) {
      state.players.push(player)
    },
    SET_PLAYERS(state, players) {
      state.players = players
    },
    SET_TEAMS(state, teams) {
      state.teams = teams
    },
    SET_PLAYERS_TOTAL(state, playersTotal) {
      state.playersTotal = playersTotal
    },
    SET_PLAYER(state, player) {
      state.player = player
    }
  },
  actions: {
    createPlayer({ commit }, player) {
      return DatabaseService.postPlayer(player).then(() => {
        commit('ADD_PLAYER', player)
      })
    },
    fetchPlayers({ commit }, { perPage, page }) {
      DatabaseService.getPlayers(perPage, page)
        .then(response => {
          commit(
            'SET_PLAYERS_TOTAL',
            parseInt(response.headers['x-total-count'])
          )
          commit('SET_PLAYERS', response.data)
        })
        .catch(error => {
          console.log(`An wild error appeared: ${error.response}`)
        })
    },
    fetchPlayer({ commit, getters }, id) {
      var player = getters.getPlayerById(id)
      if (player) {
        commit('SET_PLAYER', player)
      } else {
        console.log('An wild error appeared')
      }
    },
    fetchTeams({ commit }) {
      DatabaseService.getTeams()
        .then(response => {
          commit('SET_TEAMS', response.data)
        })
        .catch(error => {
          console.log(`An wild error appeared: ${error.response}`)
        })
    }
  },
  getters: {
    getPlayerById: state => id => {
      return state.players.find(player => player.id === id)
    }
  }
})
