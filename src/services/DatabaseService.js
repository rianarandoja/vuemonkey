import axios from 'axios'

const apiClient = axios.create({
  baseURL: `http://localhost:3000`,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  getPlayers(perPage, page) {
    return apiClient.get('/players?_limit=' + perPage + '&_page=' + page)
  },
  getPlayer(id) {
    return apiClient.get('players/' + id)
  },
  postPlayer(player) {
    return apiClient.post('/players', player)
  },
  getTeams() {
    return apiClient.get('/teams')
  },
  getTeam(id) {
    return apiClient.get('teams/' + id)
  },
  postTeam(team) {
    return apiClient.post('/teams', team)
  }
}
